package harshsinghchandrawat.com.omoto.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import harshsinghchandrawat.com.omoto.Models.AdapterModel;
import harshsinghchandrawat.com.omoto.R;

/**
 * Created by HP on 13-03-2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private Context mContext;
    private ArrayList<AdapterModel> adapterModels;
    private SubAdapter mSubRecyclerAdapter;

    public RecyclerAdapter(Context context, ArrayList<AdapterModel> adapterModels) {
        mContext = context;
        this.adapterModels = adapterModels;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rowView = inflater.inflate(R.layout.item_view, parent, false);
        ViewHolder holder = new ViewHolder(rowView);
        holder.mName = (TextView) rowView.findViewById(R.id.name);
        holder.mSubRecyclerView = (RecyclerView) rowView.findViewById(R.id.sub_recycler_view);
        holder.mProgressBar = (ImageView) rowView.findViewById(R.id.progress_bar);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder holder, final int position) {

        holder.mName.setText(adapterModels.get(position).getName());
        ArrayList<String> demo = new ArrayList<>();
        demo.add("Appointment &amp; Scheduling");
        demo.add("Minor Equipment Failure");
        demo.add("RO water related issue");
        mSubRecyclerAdapter = new SubAdapter(mContext, demo);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        holder.mSubRecyclerView.setLayoutManager(linearLayoutManager);
        holder.mSubRecyclerView.setAdapter(mSubRecyclerAdapter);
        int demo2 = adapterModels.get(position).getValue();
        int demo3 = demo2*10;
        circularImageBar(holder.mProgressBar, demo3);

    }

    private void circularImageBar(ImageView iv2, int i) {

        Bitmap b = Bitmap.createBitmap(300, 300,Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b);
        Paint paint = new Paint();

        paint.setColor(Color.parseColor("#c4c4c4"));
        paint.setStrokeWidth(10);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(150, 150, 140, paint);
        paint.setColor(Color.parseColor("#FFE82D02"));
        paint.setStrokeWidth(15);
        paint.setStyle(Paint.Style.FILL);
        final RectF oval = new RectF();
        paint.setStyle(Paint.Style.STROKE);
        oval.set(10,10,290,290);
        canvas.drawArc(oval, 270, ((i*360)/100), false, paint);
        paint.setStrokeWidth(15);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(Color.parseColor("#808080"));
        paint.setTextSize(140);
        canvas.drawText(""+i/10, 150, 150+(paint.getTextSize()/3), paint);
        iv2.setImageBitmap(b);
    }

    @Override
    public int getItemCount() {
        return adapterModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mName;
        ImageView mProgressBar;
        RecyclerView mSubRecyclerView;
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
