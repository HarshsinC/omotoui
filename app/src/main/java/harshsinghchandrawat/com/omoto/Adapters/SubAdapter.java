package harshsinghchandrawat.com.omoto.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import harshsinghchandrawat.com.omoto.Models.AdapterModel;
import harshsinghchandrawat.com.omoto.R;

/**
 * Created by HP on 13-03-2018.
 */

public class SubAdapter extends RecyclerView.Adapter<SubAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<String> mList;
    private Context mContext;
    private int mClickedId;

    public SubAdapter(Context context, ArrayList<String> adapterModels) {
        mContext = context;
        mList = adapterModels;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SubAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rowView = inflater.inflate(R.layout.sub_item_view, parent, false);
        ViewHolder holder = new ViewHolder(rowView);
        holder.mName = (TextView) rowView.findViewById(R.id.sub_name);
        holder.mRelativeLay = (RelativeLayout) rowView.findViewById(R.id.sub_view_name);
        return holder;
    }

    @Override
    public void onBindViewHolder(SubAdapter.ViewHolder holder, final int position) {
        holder.mName.setText(mList.get(position));

        if (position == mClickedId) {
            holder.mRelativeLay.setBackgroundResource(R.color.mid_grey);
        } else {

            holder.mRelativeLay.setBackgroundResource(R.drawable.drop_down3);
        }
        holder.mRelativeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyDataSetChanged();
                mClickedId = position;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mName;
        RelativeLayout mRelativeLay;
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
