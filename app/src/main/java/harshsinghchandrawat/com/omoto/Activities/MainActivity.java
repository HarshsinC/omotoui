package harshsinghchandrawat.com.omoto.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import harshsinghchandrawat.com.omoto.Adapters.RecyclerAdapter;
import harshsinghchandrawat.com.omoto.Models.AdapterModel;
import harshsinghchandrawat.com.omoto.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Spinner mSpinner;
    private RecyclerAdapter mRecyclerAdapter;
    private ArrayList<AdapterModel> adapterModels = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private RelativeLayout mLay1, mLay2, mLay3, mLay4, mLay5, mLay6, mLay7;
    private boolean click1 = false, click2, click3, click4, click5,click6, click7, click8;
    private TextView mTxt1, mTxt2, mTxt3, mTxt4, mTxt5, mTxt6, mTxt7;
    private RelativeLayout mButtomLay;
    private ImageView mDownBut;
    private TextView mFilterTxt;
    private LinearLayout mButtomLay2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        mSpinner = (Spinner) findViewById(R.id.spinner);
        mLay1 = (RelativeLayout) findViewById(R.id.value_lay1);
        mLay2 = (RelativeLayout) findViewById(R.id.value_lay2);
        mLay3 = (RelativeLayout) findViewById(R.id.value_lay3);
        mLay4 = (RelativeLayout) findViewById(R.id.value_lay4);
        mLay5 = (RelativeLayout) findViewById(R.id.value_lay5);
        mLay6 = (RelativeLayout) findViewById(R.id.value_lay6);
        mLay7 = (RelativeLayout) findViewById(R.id.value_lay7);
        mLay1.setOnClickListener(this);
        mLay2.setOnClickListener(this);
        mLay3.setOnClickListener(this);
        mLay4.setOnClickListener(this);
        mLay5.setOnClickListener(this);
        mLay6.setOnClickListener(this);
        mLay7.setOnClickListener(this);

        mTxt1 = (TextView) findViewById(R.id.value1);
        mTxt2 = (TextView) findViewById(R.id.value2);
        mTxt3 = (TextView) findViewById(R.id.value3);
        mTxt4 = (TextView) findViewById(R.id.value4);
        mTxt5 = (TextView) findViewById(R.id.value5);
        mTxt6 = (TextView) findViewById(R.id.value6);
        mTxt7 = (TextView) findViewById(R.id.value7);

        mButtomLay = (RelativeLayout) findViewById(R.id.dummy_below);
        mButtomLay2 = (LinearLayout) findViewById(R.id.dummy_below2);
        mFilterTxt = (TextView) findViewById(R.id.filter_text);
        mDownBut = (ImageView) findViewById(R.id.down_lay);
        mDownBut.setOnClickListener(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        String[] spinnerItems = new String[]{"Last 7 days      ","Last 1 month", "Yesterday"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spinnerItems);
        mSpinner.setAdapter(adapter);
        mSpinner.setSelection(0);
        addValues();
        mRecyclerAdapter = new RecyclerAdapter(MainActivity.this, adapterModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    private void addValues() {
        AdapterModel adapterModel = new AdapterModel();
        adapterModel.setName("Jack Sparrow");
        adapterModel.setValue(5);
        adapterModels.add(adapterModel);

        AdapterModel adapterModel1 = new AdapterModel();
        adapterModel1.setName("WP: 3");
        adapterModel1.setValue(2);
        adapterModels.add(adapterModel1);

        AdapterModel adapterModel2 = new AdapterModel();
        adapterModel2.setName("RFC: 5");
        adapterModel2.setValue(10);
        adapterModels.add(adapterModel2);

        AdapterModel adapterModel3 = new AdapterModel();
        adapterModel3.setName("Closed: 8");
        adapterModel3.setValue(8);
        adapterModels.add(adapterModel3);

        AdapterModel adapterModel4 = new AdapterModel();
        adapterModel4.setName("Escalated: 5");
        adapterModel4.setValue(7);
        adapterModels.add(adapterModel4);

        AdapterModel adapterModel5 = new AdapterModel();
        adapterModel5.setName("Overdue: 8");
        adapterModel5.setValue(4);
        adapterModels.add(adapterModel5);

        AdapterModel adapterModel6 = new AdapterModel();
        adapterModel6.setName("Unresolved: 8");
        adapterModel6.setValue(9);
        adapterModels.add(adapterModel6);

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.value_lay1:
                if (!click1) {
                    mLay1.setBackgroundResource(R.drawable.button_shape);
                    mTxt1.setTextColor(getResources().getColor(R.color.white));
                    click1 = true;
                } else {
                    mLay1.setBackgroundResource(R.drawable.item_bg);
                    mTxt1.setTextColor(getResources().getColor(R.color.darker_gray));
                    click1 = false;
                }
                break;

            case R.id.value_lay2:
                if (!click2) {
                    mLay2.setBackgroundResource(R.drawable.button_shape);
                    mTxt2.setTextColor(getResources().getColor(R.color.white));
                    click2 = true;
                } else {

                    mLay2.setBackgroundResource(R.drawable.item_bg);
                    mTxt2.setTextColor(getResources().getColor(R.color.darker_gray));
                    click2 = false;
                }
                break;

            case R.id.value_lay3:
                if (!click3) {
                    mLay3.setBackgroundResource(R.drawable.button_shape);
                    mTxt3.setTextColor(getResources().getColor(R.color.white));
                    click3 = true;
                } else {
                    mLay3.setBackgroundResource(R.drawable.item_bg);
                    mTxt3.setTextColor(getResources().getColor(R.color.darker_gray));
                    click3 = false;
                }
                break;

            case R.id.value_lay4:
                if (!click4) {
                    mLay4.setBackgroundResource(R.drawable.button_shape);
                    mTxt4.setTextColor(getResources().getColor(R.color.white));

                    click4 = true;
                } else {
                    mLay4.setBackgroundResource(R.drawable.item_bg);
                    mTxt4.setTextColor(getResources().getColor(R.color.darker_gray));
                    click4 = false;
                }
                break;

            case R.id.value_lay5:
                if (!click5) {
                    mLay5.setBackgroundResource(R.drawable.button_shape);
                    mTxt5.setTextColor(getResources().getColor(R.color.white));

                    click5 = true;
                } else {
                    mLay5.setBackgroundResource(R.drawable.item_bg);
                    mTxt5.setTextColor(getResources().getColor(R.color.darker_gray));
                    click5 = false;
                }
                break;

            case R.id.value_lay6:
                if (!click6) {
                    mLay6.setBackgroundResource(R.drawable.button_shape);
                    mTxt6.setTextColor(getResources().getColor(R.color.white));
                    click6 = true;
                } else {

                    mLay6.setBackgroundResource(R.drawable.item_bg);
                    mTxt6.setTextColor(getResources().getColor(R.color.darker_gray));
                    click6 = false;
                }
                break;

            case R.id.value_lay7:
                if (!click7) {
                    mLay7.setBackgroundResource(R.drawable.button_shape);
                    mTxt7.setTextColor(getResources().getColor(R.color.white));

                    click7 = true;
                } else {
                    mLay7.setBackgroundResource(R.drawable.item_bg);
                    mTxt7.setTextColor(getResources().getColor(R.color.darker_gray));
                    click7 = false;
                }
                break;

            case R.id.down_lay:
                if (!click8) {
                    mButtomLay.setVisibility(View.GONE);
                    mFilterTxt.setVisibility(View.VISIBLE);
                    mButtomLay2.setVisibility(View.VISIBLE);
                    click8 = true;
                } else {
                    mButtomLay.setVisibility(View.VISIBLE);
                    mFilterTxt.setVisibility(View.GONE);
                    mButtomLay2.setVisibility(View.GONE);
                    click8 = false;
                }
                break;
        }
    }

}
