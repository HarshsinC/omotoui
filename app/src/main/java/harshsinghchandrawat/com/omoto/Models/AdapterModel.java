package harshsinghchandrawat.com.omoto.Models;

/**
 * Created by Harsh on 13-03-2018.
 */

public class AdapterModel {

    private String name;
    private int value;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
